﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPPESRegistryAPI.Messaging
{
    public class GeneralMessages
    {
        public static readonly string HTTPSWarning = @"HTTPS is required for this website";
        public static readonly string CompanyEmail = "@companyemail.com";
    }
}
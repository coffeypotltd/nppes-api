﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NPPESRegistryAPI.Messaging
{
    public class ErrorMessages
    {
        public static readonly string AuthenticationError = @"Cannot authenticate to API Endpoint, or recieved bad data. See errors. Stacktrace: {0}. \r\n Message: {1}";
        public static readonly string APICallError = @"Cannot connect properly to API call. See errors. Stacktrace: {0} \r\n Message: {1}";
        public static readonly string NPPESError = @"Cannot connect to NPPES API, see error: {0}. Stacktrace: {1}.";
    }
}
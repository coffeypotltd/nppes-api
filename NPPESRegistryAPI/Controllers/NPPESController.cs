﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPPESRegistryAPI.APIWorker.Service;
using NPPESRegistryAPI.Logging;
using NPPESRegistryAPI.Messaging;
using NPPESRegistryAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NPPESRegistryAPI.Controllers
{
    [System.Web.Mvc.RequireHttps]
    [System.Web.Http.RoutePrefix("api/NPPES")]
    [System.Web.Mvc.Authorize]
    public class NPPESController : ApiController
    {
        private readonly IAPIService _apiService;
        private readonly ILogger _logger;

        public NPPESController(IAPIService apiService, ILogger logger)
        {
            _apiService = apiService;
            _logger = logger;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetProviderByNPI")]
        public ProviderRootObject GetProviderByNPI(string npi)
        {
            try
            {
                JObject result = _apiService.GET(string.Format(@"&number={0}", npi));
                ProviderRootObject results = JsonConvert.DeserializeObject<ProviderRootObject>(result.ToString());
                return results;
            }
            catch(Exception ex)
            {
                _logger.LogError(string.Format(ErrorMessages.APICallError, ex.StackTrace, ex.Message));
                return null;
            }
        }
    }
}
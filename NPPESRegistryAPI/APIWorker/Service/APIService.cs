﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using NPPESRegistryAPI.Logging;
using System.Net;
using System.IO;
using System.Text;
using NPPESRegistryAPI.Messaging;

namespace NPPESRegistryAPI.APIWorker.Service
{
    public class APIService : IAPIService
    {
        private ILogger _logger;
        private string _baseUrl;
        private Encoding _UTF8;

        public APIService(ILogger logger)
        {
            _logger = logger;
            _baseUrl = @"https://npiregistry.cms.hhs.gov/api/?version=2.1";
            _UTF8 = System.Text.Encoding.GetEncoding("utf-8");
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;            
        }
        
        /// <summary>
        /// Gets NPPES information based upon paramaters passed into api call.
        /// </summary>
        /// <param name="paramaters"></param>
        /// <returns></returns>
        public JObject GET(string paramaters)
        {
            WebRequest request = WebRequest.Create(_baseUrl + paramaters);
            request.Method = "GET";
            try
            {
                StreamReader reader;
                string reply;
                WebResponse response = request.GetResponse();
                reader = new StreamReader(response.GetResponseStream(), _UTF8);
                reply = reader.ReadToEnd();
                response.Close();
                string filteredReply = reply.RemoveHtml();
                return JObject.Parse(filteredReply.TrimStart(new char[] { '[' }).TrimEnd(new char[] { ']' }));
            }
            catch(Exception ex)
            {
                _logger.LogError(string.Format(ErrorMessages.NPPESError, ex.Message, ex.StackTrace));
                throw;
            }
        }
    }
}
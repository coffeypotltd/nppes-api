﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPPESRegistryAPI.APIWorker.Service
{
    public interface IAPIService
    {
        JObject GET(string path);
    }
}
